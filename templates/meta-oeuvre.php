<h5>Cartel</h5>
<div class="oeuvre_thumb"><?php the_post_thumbnail('thumbnail', array( 'class' => 'alignright' ) ); ?></div>
<div class="artist"><span class="labelrecherche">Artiste : </span><?= get_post_meta( get_the_ID(), 'artist', true ); ?></div>
<div class="titre"><span class="labelrecherche">Titre : </span><?= get_post_meta( get_the_ID(), 'title', true ); ?></div>
<div class="material"><span class="labelrecherche">Matériaux : </span><?= get_post_meta( get_the_ID(), 'materiau', true ); ?></div>
<div class="lieux"><span class="labelrecherche">Lieu : </span><?= get_post_meta( get_the_ID(), 'lieu', true ); ?></div>
<div class="date"><span class="labelrecherche">Date : </span><?= get_post_meta( get_the_ID(), 'date', true ); ?></div>
<div class="conservation"><span class="labelrecherche">Conservation : </span><?= get_post_meta( get_the_ID(), 'conservation', true ); ?></div>
<h5>Objet</h5>
<div class="type"><span class="labelrecherche">Type d'objet : </span><?= get_post_meta( get_the_ID(), 'type', true ); ?></div>
<div class="sujet"><span class="labelrecherche">Sujet : </span><?= get_post_meta( get_the_ID(), 'sujet', true ); ?></div>   
<div class="description"><span class="labelrecherche">Descripton : </span><?= get_post_meta( get_the_ID(), 'description', true ); ?></div>   
<div class="lieux"><span class="labelrecherche">Lieu d'exposition : </span><?= get_post_meta( get_the_ID(), 'exposition', true ); ?></div>   
<div class="bibliographie"><span class="labelrecherche">Bibliographie : </span><?= get_post_meta( get_the_ID(), 'bibliographie', true ); ?></div>  
<h5>Photo</h5>
<div class="estateof"><span class="labelrecherche">Estate of : </span><?= get_post_meta( get_the_ID(), 'estateof', true ); ?></div>   
<div class="copyright"><span class="labelrecherche">Copyright : </span><?= get_post_meta( get_the_ID(), 'copyright', true ); ?></div>   
<div class="reference"><span class="labelrecherche">Référence : </span><?= get_post_meta( get_the_ID(), 'reference', true ); ?></div>  