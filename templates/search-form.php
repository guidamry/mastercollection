<div>
	<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
		<div class="generic">
			<h3>Recherche générique</h3>
			<div><span class="labelrecherche">Recherche générique</span><input type="text" name="s" placeholder=""/></div>

		<hr/>

			<h3>Rechercher une oeuvre</h3>
			<div><span class="labelrecherche">Titre </span><input type="text" name="title" placeholder=""/></div>
			<div><span class="labelrecherche">Artiste </span><input type="text" name="artist" placeholder=""/></div>
			<div><?php datalist_populate('date'); ?> ou date avant <input type="text" name="datebefore" placeholder="YYYY-MM-DD"/> et/ou date après <input type="text" name="dateafter" placeholder="YYYY-MM-DD"/></div>
			<div><?php datalist_populate('materiau'); ?> ou <input type="text" name="materiausaisi" placeholder="saisir une valeur"/></div>
			<div><?php datalist_populate('lieu'); ?></div>
			<input type="hidden" name="user_query" value="1" />
			<input type="hidden" name="post_type" value="oeuvres" /> <!-- // hidden 'oeuvres' value -->
			<input type="submit" alt="Rechercher" value="Rechercher" />

		</div>
	</form>
</div>