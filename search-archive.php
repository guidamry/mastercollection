 <?php
    /* Template Name: Custom Search */        
    get_header(); ?>             
    <div class="contentarea">
        <div id="content" class="content_right">  
            <h3>Résultats pour : <?php echo "$s"; ?> </h3>       
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>    
                <div id="post-<?php the_ID(); ?>" class="posts">        
                  <?php get_template_part('/templates/content-single-oeuvre'); ?>
                </div>
    <?php endwhile; ?>
<?php endif; ?>




       </div><!-- content -->    
    </div><!-- contentarea -->   
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>