<?php
/**
 * Template Name: Oeuvre Template
 */
?>


<?php 
	$loop = new WP_Query( array( 'post_type' => 'oeuvre', 'posts_per_page' => '10','orderby' => 'title','order'   => 'ASC' ) ); ?>
	<div class="works">	
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

			<div class="work">
				<h1><a href="<?= the_permalink(); ?>"><?php the_title() ?></a></h1>
				<div><?php echo get_post_meta( get_the_ID(), 'artist', true ); ?> </div>
				<div><?php echo get_post_meta( get_the_ID(), 'material', true ); ?> </div>
				<div class="oeuvre_thumb">
					<?php the_post_thumbnail('medium', array( 'class' => 'aligncenter' ) ); ?>
				</div>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</div>

