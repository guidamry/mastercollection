<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Register Custom Post Type
function oeuvre_post_type() {

  $labels = array(
    'name'                  => _x( 'Oeuvre', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Oeuvre', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Oeuvres', 'text_domain' ),
    'name_admin_bar'        => __( 'Oeuvre', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'view_items'            => __( 'View Items', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'oeuvre', 'text_domain' ),
    'description'           => __( 'Type custom pour les fiches d\\\\\\\'oeuvres', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),

  );
  register_post_type( 'oeuvre', $args );

}
add_action( 'init', 'oeuvre_post_type', 0 );


add_filter( 'rwmb_meta_boxes', 'cartel_register_meta_boxes' );
function cartel_register_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Cartel', 'textdomain' ),
        'post_types' => [ 'oeuvre'],
        'fields'     => array(
            array(
                'id'   => 'artist',
                'name' => __( 'Artiste(s)', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'title',
                'name' => __( 'Titre', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'materiau',
                'name' => __( 'Matériau(x)', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'date',
                'name' => __( 'Année de réalisation', 'textdomain' ),
                'type' => 'date',
            ),
            array(
                'id'   => 'conservation',
                'name' => __( 'Lieu de conservation', 'textdomain' ),
                'type' => 'textarea',
            ), 
            array(
                'id'   => 'dimensions',
                'name' => __( 'Dimensions', 'textdomain' ),
                'type' => 'textarea',
            ), 
        ),
    );
    
    $meta_boxes[] = array(
        'title'      => __( 'Objet', 'textdomain' ),
        'post_types' => ['oeuvre'],
        'fields'     => array(
            array(
                'id'   => 'type',
                'name' => __( 'Type', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'sujet',
                'name' => __( 'Sujet', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'description',
                'name' => __( 'Description de l\'objet', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'exposition',
                'name' => __( 'Lieu d\'exposition ', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'bibliographie',
                'name' => __( 'Bibliographie', 'textdomain' ),
                'type' => 'textarea',
            ), 
        ),
    );   

    $meta_boxes[] = array(
        'title'      => __( 'Photo', 'textdomain' ),
        'post_types' => ['oeuvre'],
        'fields'     => array(
            array(
                'id'   => 'estateof',
                'name' => __( 'Estate Of', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'copyright',
                'name' => __( 'Copyright', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'reference',
                'name' => __( 'Référence', 'textdomain' ),
                'type' => 'text',
            ),
        ),
    );   
    return $meta_boxes;

}

$args = array(
    'flex-width'    => true,
    'width'         => 100,
    'flex-height'    => true,
    'height'        => 115,
    'default-image' => get_template_directory_uri() . '/assets/images/Logopetitchat.png',
);
add_theme_support( 'custom-header', $args );

/**
* Only oeuvres in search
*/
function remove_post_type_page_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}
add_action('init', 'remove_post_type_page_from_search');
function remove_post_type_post_from_search() {
    global $wp_post_types;
    $wp_post_types['post']->exclude_from_search = true;
}
add_action('init', 'remove_post_type_post_from_search');


/**
* Fonction de recherche d'oeuvres avancée
*/
function datalist_populate( $type ){
        $args = array(
        'post_type' => 'oeuvre',
        'meta_key'  => $type
    );
    // The Query
    $the_query = new WP_Query( $args );

    // The Loop
    if ( $the_query->have_posts() ) {
        echo '<label for="choix_'. $type .'">Indiquez votre ' . $type .' : </label>';
        echo '<select name="'. $type .'">';
        echo '<option value="" </option>';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            echo '<option value="'. get_post_meta( get_the_ID(), $type, true ) .'">'.get_post_meta( get_the_ID(), $type, true )."</option>";
        }
        echo '</select>';

        /* Restore original Post Data */
        wp_reset_postdata();
    } else {
        // no posts found
    }
}


/**
 * Extend WordPress search to include custom fields
 *
 * http://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    
    return $join;
}
add_filter('posts_join', 'cf_search_join' );


/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $wpdb;
    
    if ( is_search()) {
        //Recherche générique
        if(($_GET["s"])!="" ){
            echo "Recherche générique<br/>";
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
        }else{
            if(isset($_GET['artist']) && $_GET['artist']!== ''){
                $where = "AND (".$wpdb->postmeta.".meta_value LIKE '%".$_GET["artist"]."%' AND ".$wpdb->postmeta.".meta_key LIKE 'artist')";
            }  
            if(isset($_GET['lieu']) && $_GET['lieu']!== ''){
                $where = "AND (".$wpdb->postmeta.".meta_value LIKE '%".$_GET["lieu"]."%' AND ".$wpdb->postmeta.".meta_key LIKE 'lieu')";
            }   
            if(isset($_GET['title']) && $_GET['title']!== ''){
                $where = "AND (".$wpdb->postmeta.".meta_value LIKE '%".$_GET["title"]."%' AND ".$wpdb->postmeta.".meta_key LIKE 'title')";  
            } 
            if(isset($_GET['materiau']) && $_GET['materiau']!== ''){
                $where = "AND (".$wpdb->postmeta.".meta_value LIKE '%".$_GET["materiau"]."%' AND ".$wpdb->postmeta.".meta_key LIKE 'materiau')";               
            } 
            if(isset($_GET['materiausaisi']) && $_GET['materiausaisi']!== ''){
                $where .= " AND (".$wpdb->postmeta.".meta_value LIKE '%".$_GET["materiausaisi"]."%' AND ".$wpdb->postmeta.".meta_key LIKE 'materiau')";
            }

            if(isset($_GET['datebefore']) && $_GET['datebefore']!== ''){
                $where = "AND (".$wpdb->postmeta.".meta_value <= '".$_GET["datebefore"]."' AND ".$wpdb->postmeta.".meta_key LIKE 'date')";  
            }
            if(isset($_GET['dateafter']) && $_GET['dateafter']!== ''){ 
                $where .= " AND (".$wpdb->postmeta.".meta_value >= '".$_GET["dateafter"]."' AND ".$wpdb->postmeta.".meta_key LIKE 'date')";  
            } 
            //var_dump($where);
        }
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

